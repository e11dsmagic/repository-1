/*Para ocultar y aparecer el iframe*/

function Ocultar_iframe(seccion1, jumbotron, iframe){
    if(seccion1.style.display === "none"){
       seccion1.style.display="";
       jumbotron.style.display="";
       iframe.style.display="none";
    }
}

function Mostrar_iframe(seccion1, jumbotron, iframe){
    if(iframe.style.display === "none"){
       seccion1.style.display="none";
       jumbotron.style.display="none";
       iframe.style.display="";
    }
}